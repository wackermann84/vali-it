package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    int a = 100;
	    short b = (short)a;

	    a =b;
	    double c = a;
	    a = (int)c;

	    // Iga kassi võib võtta kui looma
        // implicit casting
        Animal animal = new Cat();

        // Iga loom ei ole kass
        // explicit casting
        Cat cat = (Cat)animal;

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        dog.setName("Naki");
        Fox fox = new Fox();





        animals.add(dog);
        dog.setName("Muki");
        animals.add(fox);
        animals.add(new Wolf());
        animals.get(animals.size()-1).setWeight(100);
        animals.add(new Pet());
        animals.get(animals.size()-1).setName("Jaan");

        fox.setBreed("Hõberebane");

        animals.get(animals.indexOf(dog)).setName("Peeter");
        dog.printInfo();

        // Kutsu kõikide listis olevate loomade printInfo välja
        for (Animal animalInList: animals) {
            animalInList.printInfo();
            System.out.println();
        }


        Animal secondAnimal = new Dog();




    }
}
