package it.vali;

public class Cat extends Pet{
    private String name;
    private String breed;
    private int age;
    private double weight;
    private boolean hasFur = true;

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }



    public void catchMouse(){
        System.out.println("Püüdsin hiire kinni");
    }

}
