package it.vali;

public class Main {

    public static void main(String[] args) {
	// Method overriding ehk meetodi ülekirjutamine
    // Tähendab seda, et päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle

        Dog buldog = new Dog();
        buldog.eat();
        buldog.getAge();

        Cat siam = new Cat();
        siam.eat();
        siam.printInfo();

        System.out.println(buldog.getAge());
        System.out.println(siam.getAge());

        // Kirjuta koera getAge üle nii, et kui koeral vanus on 0, siis näitaks 1
        // Metsloomadel printInfo võiks kirjutada Nimi:metsloomal pole nime

        Fox fox = new Fox();
        fox.printInfo();
    }
}
