package it.vali;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String,String> map = new HashMap<String, String>();

        // Sõnaraamet
        // Key => Value
        // Maja => House
        // Isa => Dad
        // Puu => Tree

        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");

        // oletame, et tahan teada, mis on inglise keeles puu

        String translation = map.get("Puu");
        System.out.println(translation);

        // Nimi ja isikukood

        Map<String,String> idNumberName = new HashMap<String, String>();
        idNumberName.put("38565453450", "Lauri");
        idNumberName.put("48565454560", "Malle");
        idNumberName.put("48565454560", "Kalle");
        // Kui kasutada put sama key lisamisel, kirjutatakse value üle


        System.out.println(idNumberName.get("48565454560"));
        //System.out.println(idNumberName.remove("48565454560"));


        // Loe lauses üle kõik erinevad tähhed ning prindi välja iga tähe järel, mitu tükki teda selles lauses oli

        String sentence = "elas metsas mutionu";
        Map<Character,Integer> letterCounts = new HashMap<Character, Integer>();

        char[] characters = sentence.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            if(letterCounts.containsKey(characters[i])){
                letterCounts.put(characters[i], letterCounts.get(characters[i])+1);
            }else{
                letterCounts.put(characters[i], 1);
            }

        }
        // Map.Entry < Character, Integer> on klass, mis hoiab endas ühte rida map-is.
        // Ehk siis ühte key-value paari

        System.out.println(letterCounts);
        for (Map.Entry<Character, Integer> entry : letterCounts.entrySet()) {
            System.out.printf("Tähte %s esines %d korda %n", entry.getKey(), entry.getValue());
        }



        // e 2
        // l 1
        // a 2 jne



    }
}
