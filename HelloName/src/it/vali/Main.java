package it.vali;

public class Main {

    public static void main(String[] args) {
        // Deklareerime/defineerime tekstitüüpi (String) muutuja (variable)
        // Mille nimeks paneme name ja väärtuseks Martin
        String name = "Martin";
        String lastName = "Wackermann";

        System.out.println("Hello " + name);
        System.out.println("Hello " + name + " " + lastName);


    }
}
