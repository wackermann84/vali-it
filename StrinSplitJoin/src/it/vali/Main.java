package it.vali;

public class Main {

    public static void main(String[] args) {

        String word = "kala";

        int length = word.length();
        System.out.println(length);


        String[] words = new String[] { "Põdral", "maja", "Metsa", "sees" };

        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

        System.out.println();
        // Prindi kõik sõnad massiivist, mis algavad m-tähega
        for (int i = 0; i < words.length; i++) {
            String firstLetter = words[i].substring(0, 1);

            if(firstLetter.toLowerCase().equals("m")) {
                System.out.println(words[i]);
            }
        }

        // Prindi kõik a tähega lõppevad sõnad
        for (int i = 0; i < words.length; i++) {
            String lastLetter = words[i].substring(words[i].length() -1) ;

            if(lastLetter.toLowerCase().equals("a")) {
                System.out.println(words[i]);
            }
        }

        // Loe üle kõik sõnad, mis sisaldavad a tähte


        // Prindi välja kõik sõnad, kus on 4 tähte
        // Prindi välja kõige pikem sõna
        // Prindi välja sõnad, kus on esimene ja viimane täht samad


    }
}
