package it.vali;

public class Main {

    // Meetodid on mingi koodi osad, mis grupeerivad mingit teatud kindlat funktsionaalsust.
    // Kui koodis on korduvaid koodi osasi, võiks mõelda, et äkki peaks nende kohta tegema eraldi meetodi.

    public static void main(String[] args) {
        printHello();
        printHello(3);
        printHello(2);
        printText("Kuidas läheb?");
        printText("Mis teed", 7);
        printText("tere",1, true);
        printText("tere",1, false);
    }

    // Lisame meetodi, mis prindib ekraanile Hello
    private static void printHello() {
        System.out.println("Hello");
    }

    // Lisame meetodi, mis prindib Hello etteantud arv kordi
    // private on default
    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println("Hello");
        }

    }
    // Lisame meetodi, mis prindib ette antud teksti välja printText
    static void printText(String text) {
        System.out.println(text);
    }

    //Lisame meetodi, mis prindib ette antud teksti välja antud arv kordi
    static void printText(String text, int howManyTimes){
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(text);
        }

    }
    static void printText(String year, int text){
        System.out.printf("%d: %s%n", year, text);

    }

    // Method Overloading - meil on mitu meedodit sama nimega, aga erineva parameetrite kombinatsiooniga
    // Meetodi ülelaadimine


    // Mis prindib ette antud teksti ette antud arv kordi.
    // lisaks saab öelda, kas tahame teha kõik tähed enne suurteks või mitte.

    static void printText(String text, int howManyTimes, boolean toUpperCase){
        for (int i = 0; i < howManyTimes; i++) {
            if(toUpperCase){
                System.out.println(text.toUpperCase());
            }else{
                System.out.println(text);
            }
        }
    }



}
