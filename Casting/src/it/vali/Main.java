package it.vali;

public class Main {

    public static void main(String[] args) {
        // Casting on pm teisendamine ühest arvutüübist teise

        byte a = 3;

        // Kui üks numbritüüp mahub teise sisse, siis toimub automaatne teisendamine. Implicit casting
        short b = a;

        // Kui üks numbritüüp ei pruugi mahtuda teise sisse, sisi peab kõigepealt ise veenduma, et see number mahub sinna teise numbri tüüpi
        // ja kui mahub, sisi peab ise seda teisendama. Explicit casting

        short c = 300;
        byte d = (byte)c;

        System.out.println(d);

        long e = 10000000000L;
        int f = (int) e;
        System.out.println(f);

        long g = f;


        float h = 123.23424F;
        double i = h;

        double j = 55.111111111111;

        // Antud juhul toimub ümardamine
        float k = (float)j;
        System.out.println(k);

        double l = 12E50; // 12*10astmel 50
        float m = (float)l;
        System.out.println(m);

        int n = 3;
        double o = n;

        double p = 2.99;
        short q = (short) p;
        System.out.println(q);

        int r = 2;
        int s = 9;

        // Int /int = int
        // Int /double = double
        // double / int = double
        // double + int = double
        // double / float = double
        System.out.println(r / (double)s);
        System.out.println(r / (float)s);

        float t = 12.55555F;
        double u = 23.555555;
        System.out.println(t / u);







    }
}
