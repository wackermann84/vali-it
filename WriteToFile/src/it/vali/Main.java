package it.vali;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        // Try plokis otsitakse/oodatakse Exceptioneid (Erind, erand, viga)
        try {
            // FileWriter on selline klass, mis tegelb faili kirjutamisega
            // sellest klassist objekti loomisel antakse talle ette faili asukoht
            // faili asukoht võib olla ainult faili nimega kirjutatud: output.txt
            // seljuhul kirjutatakse faili, mis asub samas kaustas kus meie Main.class
            // või täispika asukohaga c:\\
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");

            fileWriter.write("Elas metsas Mutionu%n");

            fileWriter.close();

        // Catch plokis püütakse kinni kindlat tüüpi exception või exceptionid, mis pärinevad
        // antud exceptionist
        //
        } catch (IOException e) {
            // printStackTrace tähendab, et prinditakse välja meetodite välja kutsumise hierarhia/ajalugu
            // e.printStackTrace();
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }
    }
}
