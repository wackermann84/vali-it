package it.vali;

import java.util.Scanner;

public class Main {
    // Kõigepealt küsitakse külastaja nime
    // kui nimi on listis, siis öeldakse kasutajale, "Tere tulemast"
    // ja küsitakse vanust
    // Kui kasutaja on alaealine, teavitatakse teda, et sorry
    // sisse ei pääse, muuljuhul siis öeldakse tere tulemast klubisse

    // Kui kasutaja ei olnud listis
    // küsitakse kasutajalt ka tema perekonnanime
    // kui perekonnanimi on listis siis öeldakse teretulemast Martin Wackermann
    // muuljuhul öeldakse "ma ei tunne sind"

    public static void main(String[] args) {
        String savedFirstName = "Martin";
        String savedLastName = "Wackermann";

        Scanner scanner = new Scanner(System.in);

        System.out.println("Tere, mis on su nimi?");
        String firstName = scanner.nextLine();

        if(savedFirstName.equals(savedFirstName)) {
            System.out.printf("Tere %s!%n", savedFirstName.toUpperCase());

            System.out.println("kui vana sa oled?");
            int age = Integer.parseInt(scanner.nextLine());
            if (age >= 18) {
                System.out.println("Tere tulemast klubisse!");

            } else
                System.out.println("Sorry, oled alaealine.");

        }







        }

    }


}
