package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    String firstString;
    String secondString;

    //4 ül
    List<Integer> years = leapYearsInCentury(1897);



    public static void main(String[] args) {
        // 1. Arvuta ringi pindala, kui teada on raadius, prindi pindala ekraanile.
        double radius = 3;
        double area = Math.PI * (radius * radius);
        System.out.printf("Ringi raadiusega %.2f pindala on %.2f%n ", radius, area);

        // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse ja mille sisendparameetriks on kaks stringi.
        // Meetod tagastab kas tõene või vale selle kohta, kas stringid võrdsed
        System.out.println(stringsToBoolean("Erinev", "Sama"));
    }

    public static boolean stringsToBoolean(String firstString, String secondString) {

        if (firstString.equals(secondString)) {
            return true;
        } else {
            return false;
        }
    }

    // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi
    // Iga sisend-massiivi elemendi kohta olgu tagastavas massiivis samapalju a tähti.
    // 3,6,7,
    // "aaa", "aaaaaa", "aaaaaaa"

    static String[] intArrayToStringArray_(int[] numbers) {
        String[] words = new String[numbers.length];
        for (int i = 0; i < words.length; i++) {
            words[i] = genereteAString(numbers[i]);
        }
        return words;
    }
    static String genereteAString(int count) {
        String word = "";
        for (int i = 0; i < count; i++) {
            word = word + "a";
            //word += "a";
        }
        return word;

    }




        // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu ja tagastab kõik sellel sajandil esinenud
        // liigaastad
        // Sisestada saab ainult aastaid vahemikus 500-2019.
        // Ütle veateade, kui aastaarv ei mahu vahemikku.



    //1799
    // start 1700
    // end 1800
    static List<Integer> leapYearsInCentury (int year) {
        if (year < 500){
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return null;
        }
        int centuryStart = year / 100*100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;

        List<Integer> years = new ArrayList<Integer>();

        for (int i = 2020; i >= centuryStart; i-=4) {
            if(i <= centuryEnd && i % 4 == 0){
                years.add(i);

            }
        }
        return years;

    }

        // 5. Defineerige klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega
        // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab
        // riikide nimekirja eraldades komaga.
        // Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega ning prindi välja
        // selle objekti toString() meetodi sisu.


}
