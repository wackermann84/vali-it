package it.vali;

public class Main {

    public static void main(String[] args) {
        String sentence = "Elas metsas Mutionu keset kuuski noori vanu";

        // sümbolite indeksid tekstis algavad samamoodi indeksiga 0, nagu massiivides

        // Leia üles esimene tühik, mis on tema indeks

        int spaceIndex = sentence.indexOf(" ");

        // indexOf tagastab -1, kui otsitavat fraasi (sümbolit või sõna) ei leitud
        // ning indeksi (kust sõna algab) kui fraas leitakse


        // Prindi välja kõigi tühikute indeksid lauses
        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        }


        // Prindi lause esimesed 4 tähte
        String part = sentence.substring(0,4);
        System.out.println(part);

        // Prindi lause teine sõna
        int indexTuhik = sentence.indexOf(" ", sentence.indexOf(" ")+1);
        part = sentence.substring(sentence.indexOf(" ")+1, indexTuhik);
        System.out.println(part);

        // Leia esimene k tähega algav sõna
        // et leiaks ka siis, kui see sõna on lause esimene sõna

        int KIndex = sentence.toLowerCase().indexOf(" k")+1;
        int tuhik = sentence.indexOf(" ")+ KIndex;
        part = sentence.substring(KIndex, tuhik+1);
        System.out.println(part);


        // Leia mitu sõna mul lauses on

        int spaceCounter =0;
        spaceIndex = sentence.indexOf(" ");
        while (spaceIndex != -1) {

            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            spaceCounter++;
        }
        System.out.println(spaceCounter + 1);


        // Leia, mitu k-tähega algavat sõna on lauses
        //





    }


}
