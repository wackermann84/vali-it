regex101.com

[0-9] {1} - tegu on 1 nr'ga 0 ja 9 vahel
[0-9] {2} - tegu on 2 nr'ga 0 ja 9 vahel

Matchib sisendi osa. Tähendab, et sisend peab sisaldama sellist regexi.
[0-9]{4}
53366

^[0-9]{4}$
^ alguse sümbol
$ lõpu sümbol
Matchib ainult nelja kohalist numbrit

^[0-9]{5,10}$
Matchib 5-10 kohalist nr

[0-9]+
Matchib 1 kuni lõpmatu kohalised 

[a-zõäöü]{1,10}
Matchib 1 kuni 10 tähelised sõnad

[a-zõäöü]+
Matchib kõik sõnad

[a-zA-ZõäöüÕÄÖÜ]+
Matchib kõik sõnad nii suurte, kui väikeste algustähtedega

.? küsimärk tähendab, et kas eelnev kas on või ei ole

[a-z0-9\._]{1,50}@[a-z0-9]{1,50}\.[a-z]{1,10}\.?[a-z]{2,10}
lauri.mattus@gmail.co.uk
Valideerib, kas on email või mitte

([a-z0-9\._]{1,50})@[a-z0-9]{1,50}\.([a-z]{1,10})\.?([a-z]{2,10})?
Sulgudega saab välja võtta meid huvitavad osad

(^\+?[0-9]{3} )?([0-9 ]{3,9})
5323523
+372 5323523
372 5323523
372 532 35 23

.+ Matchib suvalised sümbolid

[^ ]+
^ kandiliste sulgude sees tähistab eitust, Üks kuni mitu mitte-tühikut

\] (.+)
2015-08-19 00:00:12,587 (http--0.0.0.0-28080-14) [CUST:CUS85J3381] /checkSession.do in 127

|1 tähendab või