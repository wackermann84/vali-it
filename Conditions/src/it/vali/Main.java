package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    System.out.println("Sisesta number");

        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());

        // Type-safe language. Kui a on juba int, siis a ei saa enam olla nt Tere.

        if(a == 3) {
            System.out.printf("Arv %d on võrdne kolmega%n", a);
        }
        if(a < 5) {
            System.out.printf("Arv %d väiksem viiest%n", a);
        }
        if(a != 4) {
            System.out.printf("Arv %d ei võrdu neljaga%n", a);
        }
        if(a > 2) {
            System.out.printf("Arv %d on suurem kahest%n", a);
        }
        if (a >= 7) {
            System.out.printf("Arv %don suurem või võrdne seitsmega%n", a);
        }
            // ! märgiga saab kõik tingimused keerata tagurpidi
        if (!(a >= 7)) {
            System.out.printf("Arv %d ei ole suurem või võrdne seitsmega%n", a);
        }
        // arv on 2 ja 8 vahel
        if(a > 2 && a <8)    {
            System.out.printf("Arv %d on 2 ja 8 vahel%n", a);

        }

        // arv on väiksem kui 2 või arvn on suurem kui 8
        // Kui tingimuste vahel on ja, siis kui üks tingimustest leitakse, et on vale, siis edasi järgmisi tinhimusi ei vaadata.
        // kui kõikide vahel on või, siis piisab et ainult sellest.
        if(a < 2 || a > 8) {
            System.out.printf("Arv %d on väiksem kui 2 või arv on suurem kui 8%n", a);
        }

        //Kui arv on 2 ja 8 vahel või 5 ja 8 vahel või arv on suurem kui 10
        if((a > 2 && a < 8) || (a > 5 && a < 8) || a > 10) {
            System.out.printf("Arv %d on kahe ja kaheksa vahel või viie ja kaheksa vahel või suurem kui kümme%n", a);
        }


        //Kui arv ei ole 4 ja 6 vahel aga on 5 ja 8 vahel või arv on negatiivne aga pole suurem kui -14
        if((!(a>4 && a<6) && (a<5 && a<8) || (a < 0 && !(a>-14))){
            System.out.printf("Arv %d ei ole 4 ja 6 vahel aga on 5 ja 8 vahel või arv on negatiivne aga pole suurem kui -14%n", a);
        }
        //viimases on sulgudega mingi jama, üle vaadata

    }
}
