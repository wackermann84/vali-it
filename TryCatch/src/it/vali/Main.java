package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int a = 0;
        try {
            int b = 4 / a;
            String word = null;
            word.length();


        }
        // Kui on mitu catch plokki, siis otsib ta esimese ploki, mis oskab Exceptioni kinni püüda
        catch (ArithmeticException e) {
            if (e.getMessage().equals("/ by zero")) {
                System.out.println("Nulliga ei saa jagada");
            } else {
                System.out.println("Esines aritmeetiline viga");
            }


        } catch (RuntimeException e) {
            System.out.println("Esines reaalajas esinev viga");
        }
        // Exception on klass, millest kõik erinevad Exceptioni tüübid pärinevad, mis omakorda tähendab,
        // et püüdes kinni selle üldse Exceptioni, püüame kinni kõik Exceptionid
        catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Esines viga");
        }

        // Küsime kasutajalt numbri ju kui number ei ole korrektne, siis ütleme veateate

        Scanner scanner = new Scanner(System.in);

        boolean incorrectNumber;
        do {
            incorrectNumber = false;
            System.out.println("Öelge nr");
            try {
                int number = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Number oli viganei");
                incorrectNumber = true;
            }
        } while (incorrectNumber);

        // Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv. Näita veateadet.

        int[] numbers = new int[]{1, 2, 3, 4, 5};
        try {
        numbers[5] = 6;
        }   catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Indeks, kuhu tahtsid väärtust määrata, ei eksisteeri");
        }
        catch (Exception e) {
            System.out.println("Juhtus mingi tundmatu viga");
        }


    }
}
