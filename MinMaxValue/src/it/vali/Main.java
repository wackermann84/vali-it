package it.vali;

public class Main {

    public static void main(String[] args) {
	    int[] numbers = new int[] {16, -4, -10, 12, 0, 66};
	    // loeb ise, et 6 elementi kokku, new int[sulgudesse ei ole vaja ise 6 kirj.

        // Leian suurima nr
        int max = numbers [0];
        for (int i = 1; i < numbers.length; i++) {
            if(numbers[i] > max) {
                max = numbers[i];
            }
        }
        System.out.println(max);

        //Leian väikseima

        int min = numbers [0];
        for (int i = 1; i < numbers.length; i++) {
            if(numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println(min);

        // Leia suurim paaritu arv
        // Mitmes number see on?

        max = Integer.MIN_VALUE;
        int position = 0;
        boolean oddNumbersFound = false;


        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] > max && numbers[i] % 2 != 0) {
                max = numbers[i];
                position = i + 1;
                oddNumbersFound = true;
            }
        }

        // Suurim paaritu number on 5 ja ta on järjekorras number 6
        if (max != Integer.MIN_VALUE) {
            System.out.printf("Suurim paaritu number on %d ja ta on järjekorras number %d", max, position);
        }

        // Kui paarituid arve üldse ei ole, prindi, et "Paaritud arvud puuduvad"

        else {
            System.out.println("Paaritud arvud puuduvad.");
        }










    }
}
