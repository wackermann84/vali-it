package it.vali;

public interface DriverInTwoWheel extends Driver {
    void driverInRearWheel ();
}
