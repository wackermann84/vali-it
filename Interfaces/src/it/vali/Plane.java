package it.vali;


// Plane pärineb klassist Vehicle ja kasutab/implementeerib liidest Flyer
public class Plane extends Vehicle implements Flyer, Driver{
    @Override
    public void fly() {
        doCheckList();
        startEngine();
        System.out.println("Lennuk lendab");
    }
    private void doCheckList(){
        System.out.println("Täidetakse checklist");
    }
    private void startEngine(){
        System.out.println("Mootor käivitus");
    }



}
