package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Flyer bird = new Bird();
        Flyer plane = new Plane();

        bird.fly();
        System.out.println();
        plane.fly();

        List<Flyer>flyers = new ArrayList<Flyer>();
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane();
        Bird pigeon = new Bird();

        flyers.add(boeing);
        flyers.add(pigeon);

        System.out.println();


        for (Flyer flyer: flyers) {
            flyer.fly();
            System.out.println();
        }

        // Lisa liides Driver, klass Car. Mõtle, kas lennuk ja auto võiksid mõlemad kasutada Driver liidest?
        // Driver liides võiks sisaldada 3 meetodit kirjeldust:
        // int get MaxDistance()
        // void drive()
        // void stopDriving (int afterDistance)
        // Pane auto ja lennuk , mõlemad kasutama seda liidest
        // Lisa mootorratas
    }
}
