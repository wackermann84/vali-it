package it.vali;

import java.util.Scanner;

public class Main<fori> {

    public static void main(String[] args) {

        // For ja while erinevused
//        for (int i = 1; i <= 5; i++) {
//            System.out.println(i);
//        }
        // prindi 1 2 3 4 5

        int i = 1;
        while(i <= 5) {
            System.out.println(i);
            i++;
        }

        // Küsi kasutajalt mis päev täna on ja seni ta ära arvab

        Scanner scanner = new Scanner(System.in);

        String answer = "";
//        while(!answer.toLowerCase().equals("neljapäev")) {
//            System.out.println("Mis päev täna on?");
//            answer = scanner.nextLine();
//        }

        for (;!answer.toLowerCase().equals("neljapäev");) {
            System.out.println("Mis päev täna on?");
            answer = scanner.nextLine();
        }


    }
}
