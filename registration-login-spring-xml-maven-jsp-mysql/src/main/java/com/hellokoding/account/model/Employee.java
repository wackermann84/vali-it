package com.hellokoding.account.model;

public class Employee {
	private int id; 
	private String first_name; 
	private String last_name; 
	private String personal_code;  
	private boolean active;
	private long user_id;
//	private byte[] picture; 
//	
//	public byte[] getPicture() {
//		return picture;
//	}
//	public void setPicture(byte[] picture) {
//		this.picture = picture;
//	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPersonal_code() {
		return personal_code;
	}
	public void setPersonal_code(String personal_code) {
		this.personal_code = personal_code;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	  

}
