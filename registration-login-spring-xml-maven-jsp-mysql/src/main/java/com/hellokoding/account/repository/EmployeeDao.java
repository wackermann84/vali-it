package com.hellokoding.account.repository;  
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.hellokoding.account.model.Employee;  

  
public class EmployeeDao {  
JdbcTemplate template;  
	  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
	public int save(Employee p){
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("first_name", p.getFirst_name());
		mapSqlParameterSource.addValue("last_name", p.getLast_name());
		mapSqlParameterSource.addValue("personal_code", p.getPersonal_code());
		mapSqlParameterSource.addValue("active", p.isActive());		   
	    mapSqlParameterSource.addValue("user_id", p.getUser_id());
	   
	    NamedParameterJdbcTemplate jdbcTemplateObject = new 
		         NamedParameterJdbcTemplate(template.getDataSource());
	    
		String sql = "insert into Employee(first_name,last_name,personal_code, active, user_id) "
				+ "VALUES (:first_name, :last_name, :personal_code, :active, :user_id)";
	  
	    return jdbcTemplateObject.update(sql, mapSqlParameterSource);  
	}  
	public int update(Employee p){  
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("id", p.getId());
		mapSqlParameterSource.addValue("first_name", p.getFirst_name());
		mapSqlParameterSource.addValue("last_name", p.getLast_name());
		mapSqlParameterSource.addValue("personal_code", p.getPersonal_code());
		mapSqlParameterSource.addValue("active", p.isActive());		   
	    mapSqlParameterSource.addValue("user_id", p.getUser_id());

	   
	    NamedParameterJdbcTemplate jdbcTemplateObject = new 
		         NamedParameterJdbcTemplate(template.getDataSource());
	    
	    String sql = "UPDATE Employee SET id = :id, first_name = :first_name, last_name = :last_name,"
				+ " personal_code = :personal_code, active = :active,"
				+ " user_id = :user_id WHERE id = :id";

	  
	    return jdbcTemplateObject.update(sql, mapSqlParameterSource);  

	}  
	public int delete(int id){  
	    String sql="delete from employee where id="+id+"";  
	    return template.update(sql);  
	}  
	public Employee getEmployeeById(int id){  
	    String sql="select * from employee where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Employee>(Employee.class));  
	}  
	
	public Employee getEmployeeByUserId(long userId){  
	    String sql="select * from employee where user_id=?";  
	    return template.queryForObject(sql, new Object[]{userId},new BeanPropertyRowMapper<Employee>(Employee.class));  
	}  
	
	
	public List<Employee> getEmployees(){  
	    return template.query("select id, first_name, last_name, personal_code, active, user_id from employee",new RowMapper<Employee>(){  
	        public Employee mapRow(ResultSet rs, int row) throws SQLException {  
	            Employee e=new Employee();  
	            e.setId(rs.getInt(1)); 
	            e.setFirst_name(rs.getString(2)); 
	            e.setLast_name(rs.getString(3)); 
	            e.setPersonal_code(rs.getString(4));  
	            e.setActive(rs.getBoolean(5)); 
	            e.setUser_id(rs.getInt(6));
	            return e;  
	        }  
	    });  
	}  
}  