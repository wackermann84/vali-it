<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%-- <c:set var="contextPath" value="${pageContext.request.contextPath}"/> --%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome manager</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>

.left {
 text-align: center;
 width: 100px;
 color: #008B8B;
}
.center {
    text-align: center;
    vertical-align: middle;
}
td, th {
    padding: 5px;   
}
tr {
    height: 25px;   
}

</style>
<style>
	    .center {
	  text-align: center;
/* 	  color: red; */
	}
	</style>
</head>
<body>
<div >

    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome ${pageContext.request.userPrincipal.name} / Manager</h2>

    </c:if>
    </div>
    
   <div xmlns:th="http://www.thymeleaf.org"
     style="border: 1px solid #ccc;padding:5px;margin-bottom:20px;width:335px">
 
 
  <a href="user">Home</a>
 
    &nbsp; | &nbsp;
  
   <a href="viewemployee">View employees</a>
  
 	&nbsp; | &nbsp;
  
    Manager
       
    &nbsp; | &nbsp;
    
    <a onclick="document.forms['logoutForm'].submit()">Logout</a>
  
	</div>	   
     
    
    <table id="dtOrderExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
 
	<tr><th  class="center">Id</th><th  class="center">Employee id</th><th>Description</th><th  class="center">Start date</th><th  class="center">End date</th>
	<th>Is confirmed</th><th>Number of days</th><th>Number of night</th><th>Daily allowance</th><th>Total allowance</th>
	<th  class="center">Transport cost</th><th class="center">Accommodation costs</th><th class="center">Other costs</th><th>Comments</th><th class="center">Approved</th></tr>
    <c:forEach var="business_trip" items="${list}"> 
    <tr>
    <td class="center">${business_trip.id}</td>
    <td class="center" >${business_trip.employee_id}</td>
    <td>${business_trip.description}</td>
    <td class="center">${business_trip.start_date}</td>
    <td class="center">${business_trip.end_date}</td>
    <td>${business_trip.confirmed}</td>
    <td class="center">${business_trip.number_of_days}</td>
    <td class="center">${business_trip.number_of_night}</td>
    <td class="center">${business_trip.daily_allowance}</td>
    <td class="center">${business_trip.total_allowance}</td>
    <td class="center">${business_trip.transport_cost}</td>
    <td class="center">${business_trip.accommodation_costs}</td>
    <td class="center">${business_trip.other_costs}</td>
    <td>${business_trip.comments}</td>
<!--     <td class = "center"><input type="checkbox"></td> -->
    <td  class="center"><a href="saveTripApproved/${business_trip.id}">Approve</a></td>
    
    </tr>
    </c:forEach>
   
    </table>
          
<!--          <input type="submit" value="Save" class = "left" />  -->
<!--       	<a  type="submit" href="user">Back</a> -->
<!--    <input type="submit" value="Back"  -->
<!--     onclick="window.location='/account/user';" class = "left" />  -->
    
  <div xmlns:th="http://www.thymeleaf.org"
     style="text-align: right; border: 0px solid #ccc;padding:5px;margin-top:10px;width:250px">
       
    <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location.href='user'">Back</button>
 
     </div>  

<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
  
