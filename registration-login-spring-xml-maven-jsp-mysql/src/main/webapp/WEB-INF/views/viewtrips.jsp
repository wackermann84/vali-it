 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
    
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
    
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    
        <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    	<style>
	    .center {
	  text-align: center;
/* 	  color: red; */
	}
	</style>

	<h1>Employee Business Trips List</h1>
	 <table id="dtOrderExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
	<tr><th  class="center">Id</th><th class="center">Employee id</th><th>Description</th><th class="center">Start date</th><th class="center">End date</th>
	<th class="center">Is confirmed</th><th class="center">Number of days</th><th class="center">Number of nights</th><th class="center">Daily allowance</th><th class="center">Total allowance</th>
	<th class="center">Transport costs</th><th class="center">Accommodation costs</th><th class="center">Other costs</th><th>Comments</th>
	<th class="center">View</th><th class="center">Edit</th><th class="center">Delete</th><th class="center">Picture</th></tr>
    <c:forEach var="business_trip" items="${list}"> 
    <tr>
        <td>${business_trip.id}</td>
    <td class="center">${business_trip.employee_id}</td>
    <td>${business_trip.description}</td>
    <td>${business_trip.start_date}</td>
    <td>${business_trip.end_date}</td>
    <td class="center">${business_trip.confirmed}</td>
    <td class="center">${business_trip.number_of_days}</td>
    <td class="center">${business_trip.number_of_night}</td>
    <td class="center">${business_trip.daily_allowance}</td>
    <td class="center">${business_trip.total_allowance}</td>
    <td class="center">${business_trip.transport_cost}</td>
    <td class="center">${business_trip.accommodation_costs}</td>
    <td class="center">${business_trip.other_costs}</td>
    <td>${business_trip.comments}</td>
    <td class="center"><a href="viewsingletrip/${business_trip.id}">View</a></td>
    <td class="center"><a href="tripeditform/${business_trip.id}">Edit</a></td>
    <td class="center"><a href="deletebusiness_trip/${business_trip.id}">Delete</a></td>
     <td class="center"><a href="getPhoto/${business_trip.id}">Photo</a></td>
    </tr>
    </c:forEach>
    </table>
<!--     <br/> -->
    
     <div xmlns:th="http://www.thymeleaf.org"
     style="text-align: right; border: 0px solid #ccc;padding:5px;margin-top:10px;width:250px">
       
    <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location.href='travel_info_form'">Add New Business Trip</button>
     <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location.href='user'">Back</button>
 
     </div>  

<!--     <a href="travel_info_form">Add New Business Trip</a> -->
