﻿-- See on lihtne Hello World teksti päring, mis tagastab ühe rea ja ühe veeru
--(veerul puudub pealkiri). Selles veerus ja reas saab olema tekst Hello World

SELECT 'Hello World';

/*
--Täisarvu saab küsida ilma märkideta
*/

SELECT 3;

-- ei tööta PostgreSQL'is. Näitesks MSSQL'is töötab
SELECT 'raud'+'tee' 

-- Standard COMCAT töötab kõigis erinevates SQL serverites
SELECT CONCAT('all', 'maa', 'raud', 'tee', 'jaam', '.', 2, 0, 0, 4);

-- Kui tahan erinevaid veerge, siis panen väärtustele koma vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'blond'

-- AS märksõnaga saab anda antud veerule nime
SELECT 
	'Peeter' AS eesnimi,
	'Paat' AS perekonnanimi,
	23 AS vanus, 
	75.45 AS kaal, 
	'blond' AS juuksevärv

-- Tagastab väärtuse kellaaja ja kuupäeva mingis vaikimisi formaadis
SELECT NOW();

-- Kui tahan konkreetselt osa sellest, näiteks aastat või kuud
SELECT date_part('year',NOW());

-- Kui tahan ise mingit kuupäeva ette anda
SELECT date_part('month', TIMESTAMP '2019-01-01');

-- Kui tahan kellaajast minuteid
SELECT date-part('minutes' TIME '10-10');

-- Kuupäeva formaatimine Eesti kuupäeva formaati
SELECT to_char(NOW(),'HH24:mi:ss DD.MM.YYYY') 

-- Interval laseb lisada või eemaldada mingit ajaühikut
SELECT now() + interval '1 day'
SELECT now() - interval '2 days'
SELECT now() + interval '2 centuries 3 years 2 months 1 week 3 days 4 seconds';

-- Tabeli loomine
CREATE TABLE student (
	id serial PRIMARY KEY, 
	-- serial tähendab, et tüübiks on int, mis hakkabühe võrra suurenema
	-- PRIMARY KEY (primaarvõti) tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, -- Ei tohi null ollla
	last_name varchar(64) NOT NULL,
	height int NULL, -- Tohib tühi olla 
	weight numeric(5, 2) NULL,
	-- 5 nr kohtqa kokku, millest 2 on peale koma
	birthday date NULL	
);

-- Tabelist kõikide ridade ja kõikide veergude küsimine
-- * siit tähendab, et anna kõik veerud
SELECT * FROM student;

-- Kui tahan otsida/filtreerida mingi tingimuse järgi, siis on WHERE märksõna, 
-- antud juhul, et kes on 180 cm pikk
SELECT 
	* 
FROM 
	student
WHERE
	height = 180
	
-- Anna Peeter või Malle
SELECT 
	* 
FROM 
	student
WHERE
	first_name = 'Peeter'
	AND last_name = 'Tamm'
	OR middle_name = 'Malle';
	
-- Küsi tabelist eesnime ja perekonnanime järgi mingi Peeter Tamm ja mingi Mari Maasikas
SELECT 
	* 
FROM 
	student
WHERE
	(first_name = 'Peeter' AND last_name = 'Tamm') 
	OR 
	(first_name = 'Mari' AND last_name = 'Maasikas')
	
-- Anna mulle õpilased, kelle pikkus jääb 170-180 cm vahele
SELECT 
	* 
FROM 
	student
WHERE
	height >= 170 AND height <= 180
	
-- Anna mulle õpilased, kes on pikemad kui 170cm või lühemad kui 150cm
SELECT 
	* 
FROM 
	student
WHERE
	height >= 170 OR height <= 150
	
-- Anna mulle õpilaste eesnimi ja pikkus, kelle sünnipäev on jaanuaris
SELECT 
	* 
FROM 
	student
WHERE
	date_part('month',birthday)= 1
	
-- Anna mulle õpilased, kelle middle_name on null (määramata)
SELECT 
	* 
FROM 
	student
WHERE
	middle_name IS NULL
	
-- Anna mulle õpilased, kelle middle_name ei ole null
SELECT 
	* 
FROM 
	student
WHERE
	middle_name IS NOT NULL
	
-- Õpilased, kelle pikkus ei ole 180 cm
SELECT 
	* 
FROM 
	student
WHERE
	height != 180  -- Või height <> 180 või NOT (height = 180)
	
-- Anna mulle õpilased, kelle pikkus on 169, 149 või 198
SELECT 
	* 
FROM 
	student
WHERE
	height IN (169,149,198)
	
-- Anna mulle õpilased, kelle eesnimi on Peeter, Mari või kalle
SELECT 
	* 
FROM 
	student
WHERE
	-- Pikk varjant
	-- first_name = 'Peeter' OR first_name = 'Mari' OR first_name = 'Kalle'
	first_name IN ('Peeter', 'Mari', 'Kalle')
	
-- Anna õpilased, kelle eesnimi ei ole Peeter , Mari või Kalle 
SELECT 
	* 
FROM 
	student
WHERE
	-- Pikk varjant
	-- first_name = 'Peeter' OR first_name = 'Mari' OR first_name = 'Kalle'
	first_name NOT IN ('Peeter', 'Mari', 'Kalle')
	
-- Anna mulle õpilased, kelle sünnikuupäev on kuu teine, kolmas või kaheksas päev
SELECT 
	* 
FROM 
	student
WHERE
	date_part ('day', birthday) IN (2, 3, 8)
	
-- Kõik Where võrdlused jätavad välja Null väärtusega read
SELECT
	*
FROM
	student
WHERE
	height > 0 OR height <= 0 OR height IS NULL
--WHERE
	--height != 180
	
-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks
SELECT
	*
FROM
	student
ORDER BY 
	height
	
-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks, 
-- Kui pikkused on võrdsed, järjesta kaalu järgi
SELECT
	*
FROM
	student
ORDER BY 
	height, weight
	
-- Kui tahan tagurpidises järjekorras, siis lisandub sõna DESC (Descending)
-- Tegelikult ASC ( Ascending, mis on vaikeväärtus)
SELECT
	*
FROM
	student
ORDER BY 
	first_name DESC, last_name 
	
-- Anna mulle vanuse järjekorras vanemaks nooremaks, õpilaste pikkused, 
-- mis jäävad 160 ja 180 vahele
SELECT
	*
FROM
	student
WHERE
	height > 160 AND height < 180
ORDER BY 
	birthday 
	
-- Anna P tähega õpilased
SELECT
	*
FROM
	student
WHERE
	first_name LIKE 'P%' -- '%r' tähendab, et otsib r lõpevad

-- Võin otsida LIKE taha välja kitjutades ('Jaan') või alumised varjandid
SELECT
	*
FROM
	student
WHERE
	first_name LIKE 'Ka%' -- eesnimi algab 'Ka'
	OR first_name Like '%ee%' -- eesnimi sisalgab 'ee'
	OR first_name LIKE '%aan' -- eesnimi lõpeb 'aan'
	
-- Tabelisse uute kirjete lisamine
INSERT INTO student
	(first_name, last_name, height, weight, birthday, middle_name)
VALUES
	('Tiiu', 'Vihane', 175, 65.45, '1984-02-11', 'Tiia'),
	('Teet', 'Vihane', 172, 81.55, '1986-03-30', 'Mart'),
	('Siim', 'Tihane', 181, 80.55, '1981-02-21', NULL)

-- Tabelis kirje muutmine
-- UPDATE lausega peab olema ETTEVAATLIK, alati peab kasutama WHERE'i lause lõpus
UPDATE
	student
SET
	height = 199,
	weight = 100,
	
--WHERE id = 4, ku itean id nr
WHERE
	first_name = 'Peeter'
	
UPDATE
	student
SET
	height = 199,
	weight = 100,
	middle_name = 'Johnny',
	birthday = '1984-04-05'
--WHERE id = 4, ku itean id nr
WHERE
	id = 4;
	
-- Muuda kõikide õpilaste pikkus ühe võrra suuremaks
UPDATE
	student
SET
	height = height + 1
	
-- Suurenda hiljem kui 1999 sündinud õpilastel sünnipäeva ühe päeva võrra
UPDATE
	student
SET
	birthday = birthday + interval '1 day'
WHERE
	date_part('year', birthday) > 1999
	
-- Kustutamisel olla ETTEVAATLIK! Alati kasutada WHERE'id
DELETE FROM
	student
WHERE
	id = 14;
	
-- Andmete CRUD operations
-- Creat (Insert), Read (Select), Update, Delete

-- Loo uus tabel loan, millel on väljad: 
-- amount numeric (11,2), start_date, due_date, student_id
CREATE TABLE loan (
id serial PRIMARY KEY,
	amount numeric(11,2) NOT NULL,
	start_date date NOT NULL,
	due_date date NOT NULL,
	student_id int NOT NULL
);

-- Lisa neljale õpilasele laenud
-- kahele neist lisa veel üks laen
INSERT INTO loan 
	(amount, start_date, due_date, student_id)
VALUES
	(1000, NOW(), '2020-05-14', 6),
	(2300, NOW(), '2021-05-14', 7),
	(100, NOW(), '2023-05-14', 8),
	(500, NOW(), '2024-05-14', 8),
	(1200, NOW(), '2026-05-14', 9),
	(1400, NOW(), '2030-05-14', 9)
	
-- INNER JOIN -- INNER JOIN on vaikimisi join, INNER Sõna võib ära jätta
-- INNER JOIN on selline tabelite liitmine, kus liidetakse ainult need read
-- kus on võrdsed student.id = loan.student_id ehk need read, kus tabelite vahel on seos.
-- ülejaanud read ignoreeritakse
	
-- Anna mulle kõik õpilased koos laenudega
SELECT
	*
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id

-- Kui vaja tabelite järjekorda muuta, siis 
SELECT
	loan.*, student.* -- <= see rida
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
	
-- Esitada mõistlik vajalike andmetega tabel
SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
	
-- Anna õpilased laenudega aga ainult sellised laenud, mis on suuremad kui 500
-- järjesta laenu koguse järgi suuremast väiksemaks
SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
WHERE 
	loan.amount > 500
ORDER BY
	loan.amount DESC
	
-- Nime järgi ka 
SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
WHERE 
	loan.amount > 500
ORDER BY
	student.last_name, loan.amount DESC
	
-- Loo uus tabel loan_type, milles väljad name ja description
CREATE TABLE loan_type (
	id serial PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(512) NULL
);

-- Uue välja lisamine olemasolevale tabelile 
ALTER TABLE loan  -- tabeli nimi
ADD COLUMN loan_type_id int -- loan_type on nimi, int on tüüp

-- Tabeli kustutamine
DROP TABLE student;

-- Lisame mõned laenutüübid
INSERT INTO loan_type
	(name, description)
VALUES
	('Õppelaen', 'See on väga hea laen'),
	('SMS laen', 'See on väga väga halb laen'),
	('Väikelaen', 'See on kõrge intressiga laen'),
	('Kodulaen', 'See on madala intressiga laen')
	
-- Liidan 3 tabelit (INNER JOIN)
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- INNER JOINi kasutades joinimiste järjekord ei ole oluline
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	loan_type AS lt
JOIN
	loan AS l
	ON l.loan_type_id = lt.id
JOIN
	student AS s
	ON s.id = l.student_id
	
-- LEFT JOIN puhul võetakse joini esimesest (Vasakust) tabelist kõik read
-- ning teises (Paremas) tabelis näidatakse puuduvatel kohtadel NULL
-- st nt vasakust võetakse kõik read ja teisest ainult need, millel on vaste, need , 
-- millel pole neile pannakse null
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
	
-- LEFT JOIN puhul on järjekord oluline
-- Peab hakkam sealt poolt pihta, kus on rohkem andmeid, lõpetad nendega kus on vähem
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- CROSS JOIN annab kõik kombinatsioonid kahe tabeli vahel
SELECT
	s.first_name, st.first_name
FROM
	student AS s
CROSS JOIN
	student AS st
WHERE
	s.first_name != st.first_name
	
-- FULL OUTER JOIN on sama , mis LEFT JOIN + RIGHT JOIN
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN
	loan AS l
	ON s.id = l.student_id
	
-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid SMS laenu 
-- ja kelle laenu kogus on üle 100 euro, tulemused järjesta laenuvõtja vanuse järgi 
-- väiksemast suuremaks
SELECT
	s.last_name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
WHERE 
	lt.name = 'SMS laen'
	AND l.amount > 100
ORDER BY
	s.birthday
	
	
-- Aggregate functions (MIN, MAX, SUM, AVG, COUNT)
-- Agrekaatfunktsioonid selectis välja kutsudes kaob võimalus samas select lauses 
-- küsida mingit muud välja tabelist, sestagregaatfunktsiooni tulemus on alati üks number
-- ja seda ei saa kuidagi näidata koos väljadega, mida võib olla mitu rida

-- Keskmise leidmine. Jäetakse välja kus height on NULL
SELECT
	AVG(height)
FROM
	student 
	
-- Palju on üks keskmiselt laenu võtnud. Arverstatud on ka neid, kes ei ole laenu võtnud
-- (Amount on NULL). COALESCE teeb koha NULL asemele keskmise arvutamiseks antud juhul 0.
SELECT
	AVG(COALESCE(loan.amount, 0))
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
-- Ümardamine. Palju on üks keskmiselt laenu võtnud. -- Teine 0 näitab mitu komakohta. 
-- AS järele jutumärgid, mitte ülakomad
-- 
SELECT
	ROUND (AVG(COALESCE(loan.amount, 0)), 0) AS "Keskmine laenusumma", 
	MIN (loan.amount) AS "Minimaalne laenusumma",
	MAX (loan.amount) AS "Maksimaalne laenusumma",
	COUNT (*) AS "Kõikide ridade arv",
	COUNT (loan.amount) AS "LAenude arv", -- jäetakse vaälja read, kus loan.amount on NULL
	COUNT (student.height) AS "Mitmel õpilasel on pikkus kirjas"
	
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
-- Grupeerimine GROUP BY. Kasutades GROUP BY jäävad select päringu jaoks alles vaid need väljad, mis on 
-- GROUP BY's ära toodud (s.first_name, s.last_name)
-- Teisi välju saab ainult kasutada agregaatfunktsioonide sees.
-- (Jaani laenud on kokku liidetud)
SELECT
	s.first_name, s.last_name, SUM(l.amount)
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id =l.student_id
GROUP BY
	s.first_name, s.last_name
	
-- Anna mulle laenude summad laenu tüüpide järgi
SELECT
	(lt.name), SUM(l.amount)
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY
	lt.name
	
-- Anna mulle laenude summad sünniaastate järgi
SELECT
	date_part('year', s.birthday), SUM(l.amount)
FROM
	student AS s
JOIN 
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', birthday)
	
-- Anna mulle laenude, mis ületavad 1000, sünniaastate järgi
SELECT
	date_part('year', s.birthday), s.first_name, SUM(l.amount)
FROM
	student AS s
LEFT JOIN 
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', birthday), s.first_name
-- HAVING on nagu WHERE aga peale GROUP BY kasutamist
-- Filtreerimisel saad kasutada ainult neid välju, mis on GROUP BY's ja agregaatfunktsioone
HAVING
	date_part('year', s. birthday) IS NOT NULL
	AND SUM(l.amount) >= 100 -- anna ainult need read, kus summa oli suurem kui 1000
	AND COUNT(l.amount) > 0 -- anna ainult need, kus lane kokku oli 2
ORDER BY
	date_part('year', s.birthday) -- sünniaastad järjekorda
	
-- Tekita mingile õpilasele kaks sama tüüpi laenu
-- Anna mulle leanude summa grupeerituna õpilase ning laenu tüübi kaupa
-- Nimi õppelaen 2400 (nt 1200+1200)
-- Nimi väikelaen 2000
-- Nimi õppelaenu 2000
SELECT 
	s.first_name, s.last_name, lt.name, SUM(l.amount)
FROM
	student AS s
JOIN 
	loan AS l
	ON l.student_id = s.id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	s.first_name, s.last_name, lt.name
	
-- Mis aastal sündinud võtsid kõige suurema summa laenu?
SELECT
	date_part('year', s.birthday), SUM (l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON l.student_id = s.id
GROUP BY
	date_part('year', s.birthday)
ORDER BY
	SUM (l.amount) DESC
LIMIT 1 -- 1 näitab, et esitab ainult 1 rida
	
-- Anna mulle mitu laenu mingist tüübist on võetud ning mis on nende summad
-- sms laenu 	2	1000
-- lodulaen		1	2300

SELECT
	lt.name, COUNT(l.amount), SUM(l.amount)
FROM
	loan_type AS lt
JOIN
	loan AS l
	ON l.loan_type_id = lt.id
GROUP BY
 	lt.name

--Anna mulle õpilaste eesnime esitähe esinemise statistika
--ehk siis mitme õpilase eesnimi algab mingi tähega.
--m 3
--a 2
--l 4

--SUBSTRING: mis sõnast, mitmendast tähest ja mitu tähte
--algab 1st

SELECT
    SUBSTRING(s.first_name,1 ,1), COUNT(first_name)    
FROM
    student AS s
GROUP BY
    SUBSTRING(s.first_name,1 ,1)
	
-- Keskmine õpilaste pikkus ja ümardamine
SELECT
	ROUND(AVG(height))
FROM
	student
	
-- SUBQUERY or INNER QUERY or NESTED QUERY
-- Anna mulle õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele

SELECT first_name, last_name
FROM
	student
WHERE
	height =(SELECT ROUND(AVG(height)) FROM student)

-- Päringuid on võimalik päringu sisse panna
-- SUBQUERY or INNER QUERY or NESTED QUERY
-- Anna mulle õpilased,kelle eesnimi on keskmisele pikkusega õpilaste keskmine nimi
SELECT
	first_name, last_name
FROM
	student
WHERE
	first_name IN
(SELECT
	middle_name
FROM
	student
WHERE
	height =(SELECT ROUND(AVG(height)) FROM student))
	
-- Lisa kaks vanimat õpilast töötajate tabelisse
INSERT INTO employee(first_name, last_name, birthday, middle_name)
SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday LIMIT 2

-- Teisendamine täisarvuks
SELECT CAST(ROUND (AVG(age)) AS UNSIGNED) FROM emp99




	

	
	
	
	
	
	