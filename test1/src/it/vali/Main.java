package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //ylesanne1();
        ylesanne2();
        //ylesanne3();
    }

    private static void ylesanne3() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mitu arvu sa tahaksid sisestada?");
        int mituArvuSisestada = loeNumber(scanner);
        System.out.println("Sisesta arvud ühekaupa");
        int summa = 0;
        for (int i = 0; i < mituArvuSisestada; i++) {
            int a = loeNumber(scanner);
            summa = summa + a;
        }
        System.out.println("Nende arvude summa on " + summa);
        try {
            String newLine = "\r\n" + summa;
            Files.write(Paths.get("output.txt"), newLine.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }

    }

    private static void ylesanne2() {
        Scanner scanner = new Scanner(System.in);
        boolean isOdd;
        do {
            System.out.println("Sisesta esimene arv");
            int a = loeNumber(scanner);
            System.out.println("Sisesta teine arv");
            int b = loeNumber(scanner);

            int summa = a + b;
            isOdd = (summa % 2) == 0;
            if (isOdd) {
                System.out.println("Sisestage arvud nii, et nende summa ei oleks paaris arv");
            }
        } while (isOdd);
        System.out.println("Juhuuu!");

    }

    static int loeNumber(Scanner scanner) {
        do {
            String input = scanner.nextLine();
            try {
                int a = Integer.parseInt(input);
                return a;
            } catch (NumberFormatException e) {
                System.out.println("Sisesta ikkagi number");
            }
        } while (true);
    }

    private static void ylesanne1() {
        int[] intArray = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        int[] intArray2 = new int[]{1, 32, 3, 4, 5, 65, 7, 85, 9, 0};

        String numbers = getNumbers(intArray);
        System.out.println("total " + numbers);

        String numbers2 = getNumbers(intArray2);
        System.out.println("total2 " + numbers2);

        try {
            FileWriter fileWriter = new FileWriter("output.txt");
            fileWriter.append("Tere\r\n" + numbers);
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    static String getNumbers(int[] intArray) {

        boolean firstTime = true;
        String numbers = "";

        for (int i = 0; i < intArray.length; i++) {
            int currentValue = intArray[i];

            if (currentValue > 2) {
                String koma = ",";
                if (firstTime) {
                    firstTime = false;
                    koma = "";
                }
                numbers = numbers + koma + currentValue;

            }
        }

        return numbers;

    }


    // 1. Koosta täisarvude massiiv 10 arvust ning seejärel kirjuta faili kõik suuremad arvud kui 2

    // 2. Küsi kasutajalt kaks arvu. Küsi seni kuni mõlemad on korrektsed (on numbriks teisendatavad)
    // ning nende summa ei ole paaris arv.


    // Küsi kasutajalt mitu arvu ta tahab sisestada. Seejärel küsi ühe kaupa kasutajalt need arvud ning
    // kirjuta nende arvude faili. nii, et see lisatakse alati juurde.

    // 3. Küsi kasutajalt mitu arvu ta tahab sisestada
    // seejärel küsi ühe kaupa kasutajalt need arvud
    // ning kirjuta nende arvude summa faili, nii et see lisatakse alati juurde (Append)
    // Tulemus on iga programmi käivitamisel järel on failis kõik eelnevad summad kirjas.


}
