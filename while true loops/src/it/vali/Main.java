package it.vali;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {

//        while (true) {
//            System.out.println("Tere");
//        }
        // niimoodi saab teha lõppmatu tsükkel, kasutades while

        //See on koguaeg tõene ehk see tsükkel ära e ilõppe
        //nt peale igat min vaatab serverisse

        // while tsükkel on tsükkel, kus korduste arv ei ole teada

        // Näite ül
        // Programm mõtleb numbri
        // programm küsib kasutajalt arva number ära
        // SEnikaua kuni kasutaja arvab valetsi, ütleb: "Proovi uuesti"


        Random random = new Random();
        // random.nextInt(5) genereerib numbri 0 kuni 4
        int number = random.nextInt( 5 )+ 1;

        //80-100
        // int number = random.nextInt (20) + 80

        // Sama võimalus asja teha
        int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);




        //int number = 7;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Arva number 0-5");
        int guess = Integer.parseInt(scanner.nextLine());

        // while (guess != number)
        while (guess !=7) {
            System.out.println("Arva uuesti");
            guess = Integer.parseInt(scanner.nextLine());
        }
        if (guess == 7)
            System.out.println("Õige");



    }
}
