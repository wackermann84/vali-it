
package it.vali;

// Import tähendab, et antud klassile Main listakse ligipääs java class libriary paketile
// java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Loome Scanner tüübist objekti nimega scanner, mille kaudu saab kasutaja sisendit lugeda
        Scanner scanner = new Scanner(System.in);

        System.out.println("Tere, mis on sinu nimi?");
        String name = scanner.nextLine();

        System.out.println("Mis on sinu lemmik värv?");
        String color = scanner.nextLine();

        System.out.println("Mis autoga sa sõidad?");
        String car = scanner.nextLine();

        System.out.println("Sinu nimi on " + name + ". " + "Sinu lemmik värv on " + color + ". Sinu auto mark on " + car + ".");

        System.out.printf("Sinu nimi on %s. Sinu lemmik värv on %s. Sinu auto mark on %s.\n", name, color, car);

        StringBuilder builder = new StringBuilder();
        builder.append("Sinu nimi on ");
        builder.append(name);
        builder.append(". Sinu lemmik värk on ");
        builder.append(color);
        builder.append(". Sinu auto mark on ");
        builder.append(car);
        builder.append(".");

        String fullText = builder.toString();
        System.out.println(fullText);


        // nii System.out.printf kui ka String.format kasutavad enda siseselt StringBuilderit (mälule parem)

        String text = String.format("Sinu nimi on %s. Sinu lemmik värv on %s. Sinu auto mark on %s.\n", name, color, car);
        System.out.println(text);





    }
}
