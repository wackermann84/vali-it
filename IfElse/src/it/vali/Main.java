package it.vali;

public class Main {

    public static void main(String[] args) {
	    int a = 3;

	    //Kui arv on 4, siis prindi arv 4
        //muuljuhul kui arv on neg siis kontrolli kas arv on suurem, kui 10, prindi sellekohane tekst
        //muuljuhul kontrolli kas arv on suurem kui 20, pindi sellekohane tekst
	    if(a == 4) {
	        System.out.println("Arv on 4");
        }
	    else {
	        if(a<0) {
	            if(a<-10) {
	                System.out.println("Arv on väiksem kui -10");
                }
            }
	        else {
	            if (a>20) {
	                System.out.println("Arv on suurem kui 20");
                }
            }
        }


    }

}
