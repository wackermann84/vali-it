package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sum = sum(4, 5);
        System.out.printf("Arvude 4 ja 5 summa on %d%n", sum);
        System.out.printf("Arvude -12 ja -6 vahe on %d%n", subtract(-12, -6));

        int[] numbers = new int[3];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = -2;

        sum = sum(numbers);
        System.out.printf("Massiivi arvude summa on %d%n", sum);

        numbers = new int[] { 1, 2, 3, 4, 5};
        sum = sum(new int[] {2, 3, 12, 323, 43, 434, -11});
        System.out.printf("Massiivi arvude summa on %d%n", sum);
        sum = sum(new int[] {2, 3});

        printNumbers(convertToIntArray(new String[] {"2","-12","1","-45"}));
        // Alternatiiv vaatan lauri pealt


        int[] reverse = reverseNumbers(numbers);

        printNumbers(reverseNumbers(numbers));
    }

    // Meetod, mis liidab 2 täisarvu kokku ja tagastab nende summa
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    // subract
    static int subtract(int a, int b) {
        return a - b;
    }

    // multiply
    static int multiply(int a, int b) {
        return a * b;
    }

    // divide
    static double divide(int a, int b) {
        return (double)a / b;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemndid kokku ning tagastab summa
    static int sum(int[] numbers) {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }
    //Meetod, mis võtab parameetriks täisarvude massiivi, pöörab tagurpidi ja tagastab selle
    // 1 2 3 4 5
    // 5 4 3 2 1

    static int [] reverseNumbers(int[] numbers){
        int[] reverseNumbers = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            reverseNumbers[i] = numbers [numbers.length - i - 1];
        }

            return reverseNumbers;
        }
    // Meetod prindib välja täisarvude massiivi elemendid
    static void printNumbers (int[] numbers){
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }



    // Meetod, mis võtab parameetriks stringi massiivi (eeldusel, et tegelikult seal massiivis on numbrid stringidena)
    // ja teisendab numbrite massiiviks ning tagastab selle
    static int[] convertToIntArray(String[] numbersAsText) {
        int[] numbers = new int[numbersAsText.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }
        return numbers;

//        int firstNumber;
//        int secondNumber;
//        float percentage;
//
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Sisesta esimene nr");
//        firstNumber = scanner.nextInt();
//
//        System.out.println("Sisesta teine nr");
//        secondNumber = scanner.nextInt();
//
//        percentage = (firstNumber*100/secondNumber);
//
//        System.out.println("Arv"+ firstNumber + "on arvust"+ secondNumber+ percentage+ "protsenti");
    }
    // Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause, kus iga stringi vahel on tühik

    static String arrayToString(String[] sentence) {
        return String.join(" ", sentence);
    }
    // Meetod, mis võtab parameetriks stringi massiivi ning teine parameeter on sõnade eraldaja. Tagastada lause. Vaata eelmist ül, lihtsalt tühiku asemel saad ise valida, mille pealt sõnu eraldad.

    static String arrayToString(String delimiter, String[] sentence) {
        return String.join(delimiter, sentence);
    }

    // Meetod, mis leiab numbrite massiivist keskmise ning tagastab selle.
    static double average (int[] number) {
        return sum(numbers)/numbers.lenght;
    }
    // Meetod, mis arvutab mitu % moodustab esimene arv teisest.
    static double percent (int a, int b) {
        return a / (double)b*100;
    }

    // Meetod, mis leiab ringi ümbermõõdu raadiuse järgi.
    static double circumference(double radius) {
       return 2 * Math.PI*radius;
    }
}
