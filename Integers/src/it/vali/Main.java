package it.vali;

public class Main {

    public static void main(String[] args) {

        // Muutujat deklareerida ja talle väärtust anda saab ka kahe sammuga
        String word;
        word = "Kala";

        int number;
        number = 3;

        int secondNumber = -7;

        System.out.println(number);
        System.out.println(secondNumber);

        System.out.println(number + secondNumber);

        //Arvude 3 ja -7 korrutis on -21
        // %n tekitab platvormi spetsiifilise reavahetuse (Windows \r\n ja Linux/Mac \n)
        // selle asemel saab kasutada ka System.lineSeparator()

        int a = 3;
        int b = 5;
        //String + number on alati string
        // Stringi liitmisel numbriga, teisendatakse number stringiks ja liidetakse kui liitsõna

        System.out.println("Arvude summa on " + (a + b));

        //Kahe täisarvu jagamisel on tulemuse jagatise täisosa ehk kõik peale süüakse ära


        System.out.println(number * secondNumber);
        System.out.printf("arvude %d ja %d summa on %d.", number, secondNumber, number + secondNumber);


        System.out.printf("Arvude %d ja %d jagatis on %d%n", a, b, a/b);
        System.out.printf("Arvude %d ja %d jagatis on %d%n", b, a, b/a);

        int maxInt = 2147483647;
        int c = maxInt + 1;
        int d = maxInt + 2;
        System.out.println(c);
        System.out.println(d);

        int minInt = -2147483648;
        int e = minInt - 1;
        System.out.println(e);

        short f = 32199;
        // long väärtust andes peab L tähe lõppu lisama
        long g = 9999999999L;
        long h = 234;
        System.out.println(g+h);




    }
}
