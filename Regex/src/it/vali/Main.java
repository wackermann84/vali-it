package it.vali;

import java.util.Calendar;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
//        System.out.println("Sisesta email");
////        Scanner scanner = new Scanner(System.in);
////
////        String email = scanner.nextLine();
////        String regex = "(([a-z0-9\\\\._]{1,50})@[a-z0-9]{1,50}\\\\.([a-z]{1,10})\\\\.?([a-z]{2,10}?)\"));
////        if (email.matches(regex)){
////            System.out.println("Email oli korrektne");
////        }else{
////            System.out.println("Email ei olnud korrektne");
////            // niimoodi saab valideerida
////
////        Matcher matcher = Pattern.compile(regex).matcher(email);
////        if(matcher.matches()) {
////            System.out.println("Kogu email: " + matcher.group(0));
////            System.out.println("Tekst vasakulpool @ märki: " + matcher.group(1));
////            System.out.println("Domeeni laiend: " + matcher.group(2));
////        }



        // Küsi kasutajalt isikukood ja valideeri, kas see on õiges formaadis
// Mõelge ise mis piirangud isikukoodis peaks olema
// aasta peab olema reaalne aasta, kuu number, kuupäeva number
// Küsi kasutajalt isikukood ja prindi välja tema sünnipäev

            System.out.println("Sisesta isikukood: ");
            Scanner scanner = new Scanner(System.in);
            String ID = scanner.nextLine();
            String regexID = "^[1-6]([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[01])[0-9]{4}$";

            if (ID.matches(regexID)){
                System.out.println("Isikukood on korrektne");
            } else {
                System.out.println("Isikukood ei olnud korrektne");
            }

            Matcher matcher = Pattern.compile(regexID).matcher(ID);
            if (matcher.matches()) {
                int year = Calendar.getInstance().get(Calendar.YEAR);
                System.out.println("Sinu sünnipäev on: " + matcher.group(2) + "." + matcher.group(3) + "." + year);

            }


    }

}

