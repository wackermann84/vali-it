package it.vali;

public class Main {

    public static void main(String[] args) {
	// write your code here
        LivingPlace livingPlace = new Farm();

        Pig pig = new Pig();
        Cow cow = new Cow();



        livingPlace.addAnimal(new Cat());
        livingPlace.addAnimal((new Horse()));
        livingPlace.addAnimal(new Pig());
        // pig.setName("Kalle"); Näited
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(new Cow());
        // cow.setName("Priit");

        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Cow");
        System.out.println();
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();





    }
}
