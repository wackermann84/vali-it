package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm implements LivingPlace{
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();

    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, palju meil igat looma farmi mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Farm(){
        maxAnimalCounts.put("Pig", 2);
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Sheep", 3);
    }
    // Võiks olla võimalus farmiloomi lisada, selleks tuleb teha meetod
    @Override
    public void addAnimal(Animal animal){
        // Sellega me kontrollime, kas animal on tüübist FarmAnimal või pärineb sellest tüübist
        if(!FarmAnimal.class.isInstance(animal)) {
            System.out.println("Farmis saavad elada ainult farmiloomad");
            return;
        }

        String animalType = animal.getClass().getSimpleName();



        if(!maxAnimalCounts.containsKey(animalType)){
            System.out.println("Farmis üldse sellistele loomadele kohta pole");
            return;
        }


        if(animalCounts.containsKey(animalType)){
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount){
                System.out.println("Farmis on sellele loomale kõik kohad juba täis");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma ei ole farmis
            // kindlasti sellele loomale kohta on
        }else{
           animalCounts.put(animalType, 1);
        }
        animals.add((FarmAnimal)animal);
        System.out.printf("Farmi lisati loom %s%n", animalType);



    }
    // Tee meetod, mis prindib välja kõik farmis elavad loomad ning mitu neid on
    @Override
    public void printAnimalCounts(){
        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }
    // Tee meetod, mis eemaldab farmist looma
    @Override
    public  void removeAnimal(String animalType){
        //Kuidas leida nimekirjast loom, kelle nimi on Kalle
        //Teen muutuja, mis ma tahan, et iga element listist oleks: list mida tahan läbi käia
        // FarmAnimal animal hakkab olema järjest esimane loom,
        // siis teine loom, kolmas ja seni kuni loomi on
        boolean animalFound = false;

        for (FarmAnimal animal: animals) {
            if (animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                // kui tahan , et kustutaks esimese, siis break, st et leiab esimese ja lõpetab
                System.out.printf("Farmist eemaldati loom %s%n", animalType);

                // Kui see oli viimane loom, siis eemalda see rida animalCpunts mapist
                // muuljuhul vähenda animalCounts map-is seda kogust
                if(animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                }else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;
            }else{
                System.out.println("Looma puudub");
                break;
            }
        }
        if(!animalFound){
            System.out.println("Farmis antud loom puudub");
        }

        // For tsükliga sama
//        for (int i = 0; i < animals.size(); i++) {
//            if (animals.get(i).getClass().getSimpleName().equals(animalType)){
//                animals.remove(animals.get(i));
//            }
//
//        }
        // täienda meedodit nii, et kui ei leitud ühtegi sellest tüübist looma
        // prindi "Farmis selline loom puudub"
    }

}
