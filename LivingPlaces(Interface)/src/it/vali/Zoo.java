package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();

    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Siin hoitakse infot, palju meil igat looma farmi mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Zoo(){
        maxAnimalCounts.put("Wolf", 2);
        maxAnimalCounts.put("Fox", 3);

    }


    @Override
    public void addAnimal(Animal animal) {

    }

    @Override
    public void printAnimalCounts() {

    }

    @Override
    public void removeAnimal(String animalType) {

    }
}
