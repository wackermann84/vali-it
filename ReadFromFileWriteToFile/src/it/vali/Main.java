package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
        // loe failist input.txt iga teine rida ning kirjuta need read
        // faili output.txt

        try {
            // Notepad vaikimisi kasutab ANSI encoding-ut. Selleks, et FileReader oskaks
            // seda korrektselt lugeda (täpitähti) peame talle ette ütlema, et loe seda ANSI encodingus
            // Cp1252 on javas ANSI encoding
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt", Charset.forName("Cp1252"));

            // Faili kirjutades on javas vaikeväärtus UTF-8
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt", Charset.forName("UTF-8"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();

            int lineNumber = 1;
            while (line != null) {
                if (lineNumber % 2 == 1) {
                    fileWriter.write(line + System.lineSeparator());
                }
                line = bufferedReader.readLine();
                lineNumber++;

                fileReader.close();
                fileWriter.close();
                bufferedReader.close();
            }
            } catch(IOException e){
                e.printStackTrace();
            }


        }
    }
}

