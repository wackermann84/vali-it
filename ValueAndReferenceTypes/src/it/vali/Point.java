package it.vali;

public class Point {
    public int x;
    public int y;


    public void printNotStatic (){
        System.out.println("Olen punkt");
    }

    public static void printStatic (){
        System.out.println("Olen static punkt");
    }

    public void increment(){
        x++;
        y++;
    }

}
