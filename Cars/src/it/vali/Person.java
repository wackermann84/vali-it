package it.vali;

enum Gender {
    FEMALE, MALE, NOTSPECIFIED
}

public class Person {
    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;

    // Kui klassil ei ole defineeritud konstruktorit, siis tegelikult tehakse
    // nähtamatu parameetrita konstruktor, mille sisu on tühi
    // Kui klassile ise lisada mingi konstruktor, siis see nähtamatu parameetrita
    // konstruktor kustutatakse
    // Sellest klassist saab siis teha objekti ainult selle uue konstruktoriga

    public Person(String firstName, String lastName, Gender gender, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
    }

    public void whoseCar (String firstName) {
        if (firstName.equals("Peeter")) {
            System.out.println("VW");
        } else if (firstName.equals("Priit")){
            System.out.println("Ford");
        } else if (firstName.equals("Raul")) {
            System.out.println("Kia");
        } else {
            System.out.println("Puudub sellise nimega kasutaja!");
        }
    }

}
