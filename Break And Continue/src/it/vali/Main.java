package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Küsime kasutajalt pin koodi,
        // kui see on õige , siis ütleme "Tore".
        // Muul juhul küsime uuesti.
        // Kokku küsime 3 korda

        String realPin = "1234";
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisesta PIN kood?");
            String enteredPin = scanner.nextLine();

            if (enteredPin.equals(realPin)) {
                System.out.println("Tore, õige pin kood");
                break;
                // Break hüppab tsüklist välja
            }
        }

        System.out.println();

        int triesLeft = 3;
        do {
            System.out.println("Palun sisesta PIN kood?");
            String enteredPin = scanner.nextLine();
            triesLeft--;
            if (enteredPin.equals(realPin)) {
                System.out.println("Tore, õige pin kood");
            }
            if (triesLeft == 0) {
                break;
            }
        } while (scanner.nextLine().equals(realPin) && triesLeft > 0);

        if (triesLeft > 0) {
            System.out.println("Panid 3 korda valetsi");
        } else {
            System.out.println("Tore, õige pin kood");

            // poolik aga mõtekam on breaki kasutada
        }

        // Continue
        // prindi välja 10 kuni 20 ja 40 kuni 60
        // Contoniue jätab selle tsüklikorduse katki ja läheb järgmise korduse juurde
        for (int i = 10; i <= 60; i++) {
            if (i > 20 && i < 40)
                continue;

        }
        System.out.println(i);

        }

    }


